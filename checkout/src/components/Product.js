import React from "react";

function Products(props) {
  const { products, onAdd, onRemove } = props;
  return (
    <div>
      {products.map((item, index) => (
        <div key={index}>
          <p>{item.name}</p>
          <p>{item.price}</p>
          <p>{item.quantity}</p>
          <button onClick={() => onAdd(item)}>+</button>
          <button onClick={() => onRemove(item)}>-</button>
        </div>
      ))}
    </div>
  );
}

export default Products;
