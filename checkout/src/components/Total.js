import React from "react";

function Total(props) {
  const { total } = props;

  return <h1>Total: {total}</h1>;
}

export default Total;
