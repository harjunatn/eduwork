export const products = [
  {
    id: "1",
    name: "Laptop",
    price: 1000,
    quantity: 10,
  },
  {
    id: "2",
    name: "Mobil",
    price: 1000,
    quantity: 5,
  },
  {
    id: "3",
    name: "Motor",
    price: 1000,
    quantity: 7,
  },
  {
    id: "4",
    name: "Sepeda",
    price: 1000,
    quantity: 2,
  },
  {
    id: "5",
    name: "Monitor",
    price: 1000,
    quantity: 8,
  },
];
