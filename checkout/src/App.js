import React, { useState } from "react";

import Products from "./components/Product";
import Total from "./components/Total";
import { products } from "./data";

function App() {
  const [data, setData] = useState(products);
  const total = data.reduce(
    (current, total) => current + total.price * total.quantity,
    0
  );

  function onAdd(item) {
    const exist = data.find((x) => x.id === item.id);
    if (exist) {
      setData(
        data.map((x) =>
          x.id === item.id ? { ...exist, quantity: exist.quantity + 1 } : x
        )
      );
    }
  }

  function onRemove(item) {
    const exist = data.find((x) => x.id === item.id);
    if (exist.quantity === 1) {
      setData(data.filter((x) => x.id !== item.id));
    } else {
      setData(
        data.map((x) =>
          x.id === item.id ? { ...exist, quantity: exist.quantity - 1 } : x
        )
      );
    }
  }

  return (
    <div className="App">
      <Products products={data} onAdd={onAdd} onRemove={onRemove} />
      <Total total={total} />
    </div>
  );
}

export default App;
